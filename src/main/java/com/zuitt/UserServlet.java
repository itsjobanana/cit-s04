package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	
	public void init() throws ServletException{
		System.out.println("********************************");
		System.out.println("Information has been initialized");
		System.out.println("********************************");
	}
	
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		//getting input from form to storing it in a variable
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String email = req.getParameter("email");
		String contact = req.getParameter("contact");
		
		//system properties
		System.getProperties().put("firstname", firstname);
		
		//http session
		HttpSession session = req.getSession();
        session.setAttribute("lastname", lastname);
        
        //context setAttribute
        getServletContext().setAttribute("email", email);

        
        // Url Rewriting via sendRedirect Method
        res.sendRedirect("details?contact="+contact);


	}
	
	
	
	public void destory() {
		System.out.println("******************************");
		System.out.println("Information has been destoryed");
		System.out.println("******************************");
	}

}
