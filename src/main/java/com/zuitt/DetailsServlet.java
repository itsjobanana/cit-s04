package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	
	
	public void init() throws ServletException{
		System.out.println("********************************");
		System.out.println("Information has been initialized");
		System.out.println("********************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)throws IOException, ServletException{
		
		HttpSession session = req.getSession();
		ServletContext srvContext = getServletContext();
		
		String branding = srvContext.getInitParameter("Branding");
		String firstname = System.getProperty("firstname");
		String lastname = session.getAttribute("lastname").toString();
		String email = srvContext.getAttribute("email").toString();
		String contact = req.getParameter("contact");
		
		PrintWriter out = res.getWriter();
        
	    
	    out.println(
					
			"<h1>"+ branding +"</h1>" +
			"<p> First Name: " + firstname + "</p>"+
			"<p> Last Name: " + lastname + "</p>" +
			"<p> Email: " + email + "</p>" +
			"<p> Contact: " + contact + "</p>"
					
		);
	     




	}

	
	
	public void destory() {
		System.out.println("******************************");
		System.out.println("Information has been destoryed");
		System.out.println("******************************");
	}

}
